<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Genres extends Model
{
    use HasFactory;
    protected $table = 'genres';

    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Books::class, 'book_genre', 'genre_id', 'book_id');
    }
}
