<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Books extends Model
{
    use HasFactory;
    protected $table = 'books';
    protected $fillable = ['name'];

    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genres::class, 'book_genre', 'book_id', 'genre_id');
    }

    public function author()
    {
        return $this->belongsTo(Authors::class);
    }

    public function getCreatedAtAttribute()
    {
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d.m.Y');
        return $date; 
    }

    public function getBooksWithGenre($genre) {
        return $this->belongsToMany(Genres::class, 'book_genre', 'book_id', 'genre_id')->wherePivot('genre_id', $genre);
    }
}
