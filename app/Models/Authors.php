<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    use HasFactory;
    protected $table = 'authors';
    protected $fillable = ['name'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function books() {
        return $this->hasMany('App\Models\Books', 'author_id');
    }

    public function getCountBooks() {
        return $this->hasMany('App\Models\Books', 'author_id')->count();   
    }
}
