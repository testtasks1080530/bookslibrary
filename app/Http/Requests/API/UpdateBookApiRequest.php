<?php

namespace App\Http\Requests\API;

use App\Enums\BookTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateBookApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['min:2','max:255'],
            'type' => [new Enum(BookTypes::class)],
            'author_id' => ['min:1', 'exists:authors,id']
        ];
    }
}
