<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGenreRequest;
use App\Http\Requests\UpdateGenreRequest;
use App\Models\Genres;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function index()
    {
        $genres = Genres::all();
        return view('genreViews.genres', compact('genres'));
    }

    public function show(Genres $genre)
    {
        return view('genreViews.genre', compact('genre'));
    }

    public function store(StoreGenreRequest $request)
    {
        $validated = $request->validated();

        $genre = new Genres();
        $genre->name = $request->name;
        $genre->save();
        $newGenreId = Genres::firstWhere('name', $request->name)->id;

        return redirect()->route('genres.show', ['genre' => $newGenreId]);
    }

    public function create()
    {
        return view(
            'genreViews.genre-crud',
            [
                'type' => 'create'
            ]
        );
    }

    public function update(Genres $genre)
    {
        return view(
            'genreViews.genre-crud',
            [
                'type' => 'update',
                'genre' => $genre
            ]
        );
    }

    public function delete(Genres $genre)
    {
        return view(
            'genreViews.genre-crud',
            [
                'type' => 'delete',
                'genre' => $genre,
            ]
        );
    }

    public function destroy(Genres $genre)
    {
        $genreDel = Genres::find($genre->id);
        $genreDel->delete();

        return redirect()->route('genres.index');
    }

    public function save(UpdateGenreRequest $request)
    {
        $validated = $request->validated();

        $genreId = $request->route('genre');
        $genre = Genres::find($genreId);
        $genre->name = $request->name;
        $genre->save();

        return redirect()->route('genres.show', ['genre' => $genreId]);
    }
}
