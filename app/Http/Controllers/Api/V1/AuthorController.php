<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\UpdateAuthorApiRequest;
use App\Http\Resources\API\AuthorAllResource;
use App\Http\Resources\API\AuthorResource;
use App\Models\Authors;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Authors::paginate();
        return AuthorAllResource::collection($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = Authors::findOrFail($id);
        return new AuthorResource($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAuthorApiRequest $request, $id)
    {
        if (auth()->user()->tokenCan($id)) {
            $author = Authors::find($id);
            $author->update($request->validated());
            return new AuthorResource($author);
        }
        return response()->json([
            'message' => 'No access rights',
        ], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->tokenCan($id)) {
            $author = Authors::find($id);
            $author->delete();
            return response(null, Response::HTTP_NO_CONTENT);
        }
        return response()->json([
            'message' => 'No access rights',
        ], 401);
    }
}
