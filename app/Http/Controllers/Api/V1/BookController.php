<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\API\UpdateBookApiRequest;
use App\Http\Resources\API\BookResource;
use App\Models\Authors;
use App\Models\Books;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Books::paginate();
        return BookResource::collection($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = Books::findOrFail($id);
        Log::channel('booksLogs')->info('SHOW::API', ['id' => $id]);
        return new BookResource($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookApiRequest $request, $id)
    {
        $request->validated();
        $book = Books::find($id);
        $authorId = $book->author_id;
        if (auth()->user()->tokenCan($authorId)) {
            if ($request->name) {
                $book->name = $request->name;
            }
            if ($request->type) {
                $book->type = $request->type;
            }
            if ($request->author_id) {
                $book->author_id = $request->author_id;
            }
            $book->save();
            Log::channel('booksLogs')->info('UPDATE::API', ['id' => $id, 'request' => $request->validated()]);
            return new BookResource($book);
        }
        return response()->json([
            'message' => 'No access rights',
        ], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Books::find($id);
        $authorId = $book->author_id;
        if (auth()->user()->tokenCan($authorId)) {
            $book->delete();
            Log::channel('booksLogs')->info('DELETE::API', ['id' => $id]);
            return response(null, Response::HTTP_NO_CONTENT);
        }
        return response()->json([
            'message' => 'No access rights',
        ], 401);
    }
}
