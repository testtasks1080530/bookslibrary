<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Http\Resources\BookCollection;
use App\Http\Resources\BookResource;
use App\Models\Authors;
use App\Models\BookGenre;
use App\Models\Books;
use App\Models\Genres;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PharIo\Manifest\Author;

class BookController extends Controller
{
    public function index()
    {
        $books = Books::all();
        return view('bookViews.books', ['books' => $books, 'authors' => Authors::all(), 'genres' => Genres::all()]);
    }

    public function show(Books $book)
    {
        Log::channel('booksLogs')->info('SHOW', ['id' => $book->id]);
        return view('bookViews.book', compact('book'));
    }

    public function store(StoreBookRequest $request)
    {
        $request->validated();
        $idAuthor = Authors::where('name', $request->author)->get()[0]->id;

        // create new Book
        $book = new Books;
        $book->name = $request->name;
        $book->type = $request->type;
        $book->author_id = $idAuthor;
        $book->save();
        Log::channel('booksLogs')->info('CREATE', ['name' => $request->name, 'type' => $request->type, 'author_id' => $idAuthor]);
        $newBookId = Books::firstWhere('name', $request->name)->id;

        // create new BookGenre
        $req = $request->except(['_method', '_token', 'name', 'type', 'author']);
        foreach ($req as $key => $value) {
            $genreNew = new BookGenre;
            $genreNew->book_id = $newBookId;
            $genreNew->genre_id = $value;
            Log::channel('booksLogs')->info('BOOK_GENRE::CREATE', ['book_id' => $newBookId, 'genre_id' => $value]);
            $genreNew->save();
        }

        return redirect()->route('books.show', ['book' => $newBookId]);
    }

    public function create()
    {
        $genresAll = Genres::all();
        return view(
            'bookViews.book-crud',
            [
                'type' => 'create',
                'genresAll' => $genresAll
            ]
        );
    }

    public function update(Books $book)
    {
        $author = Authors::find($book->author_id);
        $genresId = BookGenre::where('book_id', $book->id)->get('genre_id');
        $genresUnchecked = Genres::all()->diff(Genres::wherein('id', $genresId)->get());
        $genresAll = Genres::all();
        return view(
            'bookViews.book-crud',
            [
                'type' => 'update',
                'book' => $book,
                'author' => $author,
                'genresUnchecked' => $genresUnchecked,
                'genresAll' => $genresAll
            ]
        );
    }

    public function delete(Books $book)
    {
        return view(
            'bookViews.book-crud',
            [
                'type' => 'delete',
                'book' => $book,
            ]
        );
    }

    public function destroy(Books $book)
    {
        $bookDel = Books::find($book->id);
        $bookDel->delete();

        Log::channel('booksLogs')->info('DELETE', ['id' => $book->id]);
        return redirect()->route('books.index');
    }

    public function save(UpdateBookRequest $request)
    {
        $request->validated();
        // edit Book
        $idAuthor = Authors::where('name', $request->author)->get()[0]->id;
        $bookId = $request->route('book');
        $book = Books::find($bookId);
        $book->name = $request->name;
        $book->type = $request->type;
        $book->author_id = $idAuthor;
        Log::channel('booksLogs')->info('UPDATE', ['id' => $bookId, 'request' => $request->validated()]);
        $book->save();

        // edit BookGenre
        $req = $request->except(['_method', '_token', 'name', 'type', 'author']);
        $bookGenreAll = BookGenre::where('book_id', $bookId)->get();
        foreach ($bookGenreAll as $bookGenre) {
            $bookGenreDel = BookGenre::find($bookGenre->id);
            Log::channel('booksLogs')->info('BOOK_GENRE::DELETE', ['id_bookGenre' => $bookGenre->id]);
            $bookGenreDel->delete();
        }
        foreach ($req as $key => $value) {
            $genreNew = new BookGenre;
            $genreNew->book_id = $bookId;
            $genreNew->genre_id = $value;
            Log::channel('booksLogs')->info('BOOK_GENRE::CREATE', ['book_id' => $bookId, 'genre_id' => $value]);
            $genreNew->save();
        }
        return redirect()->route('books.show', ['book' => $bookId]);
    }

    public function search(Request $request)
    {
        // search name book
        $search = $request->search_book;
        $booksFilter = Books::all();
        $searchedBooks = $booksFilter->filter(function ($item) use ($search) {
            return stripos($item['name'], $search) !== false;
        });

        // filter author
        $filterAuthor = $request->filter_author;
        $filteredAuthors = [];
        if ($filterAuthor === "all") {
            $filteredAuthors = $searchedBooks;
        } else {
            $filteredAuthors = $searchedBooks->where('author_id', $filterAuthor);
        }

        // filter genre
        $filterGenre = $request->filter_genre;
        $filteredGenres = [];
        if ($filterGenre === "all") {
            $filteredGenres = $filteredAuthors;
        } else {
            $bookGenres = BookGenre::all()->where('genre_id', $filterGenre);
            if (!$bookGenres->isEmpty()) {
                $filteredGenres = $filteredAuthors->whereIn('id', $bookGenres->pluck('book_id')->toArray());
            }
        }

        // sort name book
        if ($request->sort_book && !empty($filteredGenres)) {
            $sortBooks = $filteredGenres->sortBy('name');
        } else {
            $sortBooks = $filteredGenres;
        }
        
        $books = $sortBooks;

        return view('bookViews.books', ['books' => $books, 'authors' => Authors::all(), 'genres' => Genres::all()]);
    }
}
