<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    private $links = ['books'=>'Книги','genres'=>'Жанры','authors'=>'Авторы'];
    public function index() {
        $links = $this->links;
        return view('main', compact('links'));
    }
}
