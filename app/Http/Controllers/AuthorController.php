<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;
use App\Models\Authors;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index()
    {
        $authors = Authors::withCount('books')->get();
        return view('authorViews.authors', compact('authors'));
    }

    public function show(Authors $author)
    {   
        return view('authorViews.author', compact('author'));
    }

    public function store(StoreAuthorRequest $request)
    {
        $validated = $request->validated();

        $author = new Authors;
        $author->name = $request->name;
        $author->save();
        $newAuthorId = Authors::firstWhere('name', $request->name)->id;

        return redirect()->route('authors.show', ['author' => $newAuthorId]);
    }

    public function create()
    {
        return view(
            'authorViews.author-crud',
            [
                'type' => 'create'
            ]
        );
    }

    public function update(Authors $author)
    {
        return view(
            'authorViews.author-crud',
            [
                'type' => 'update',
                'author' => $author
            ]
        );
    }

    public function delete(Authors $author)
    {
        return view(
            'authorViews.author-crud',
            [
                'type' => 'delete',
                'author' => $author,
            ]
        );
    }

    public function destroy(Authors $author)
    {
        $authorDel = Authors::find($author->id);
        $authorDel->delete();

        return redirect()->route('authors.index');
    }

    public function save(UpdateAuthorRequest $request)
    {
        $validated = $request->validated();

        $authorId = $request->route('author');
        $author = Authors::find($authorId);
        $author->name = $request->name;
        $author->save();

        return redirect()->route('authors.show', ['author' => $authorId]);
    }
}
