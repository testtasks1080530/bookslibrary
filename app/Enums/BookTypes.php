<?php

namespace App\Enums;

enum BookTypes: string
{
    case Graphic = 'graphic';
    case Print = 'print';
    case Digital = 'digital';  
}
