<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ProfileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [MainController::class, 'index'])->middleware(['auth', 'verified'])->name('main.index');

Route::controller(BookController::class)->group(function () {
    Route::middleware(['auth', 'verified'])->group(function () {
        Route::get('/books', 'index')->name('books.index');
        Route::get('/books/{book}', 'show')->name('books.show');
        Route::get('/books/{book}/update', 'update')->name('books.update');
        Route::get('/books/{book}/delete', 'delete')->name('books.delete');
        Route::delete('/books/{book}/destroy', 'destroy')->name('books.destroy');
        Route::get('/book/create', 'create')->name('book.create');
        Route::post('/books/store', 'store')->name('books.store');
        Route::put('/books/{book}/save', 'save')->name('books.save');
        Route::get('/book/search', 'search')->name('books.search');
    });
});

Route::controller(AuthorController::class)->group(function () {
    Route::middleware(['auth', 'verified'])->group(function () {
        Route::get('/authors', 'index')->name('authors.index');
        Route::get('/authors/{author}', 'show')->name('authors.show');
        Route::get('/authors/{author}/update', 'update')->name('authors.update');
        Route::get('/authors/{author}/delete', 'delete')->name('authors.delete');
        Route::delete('/authors/{author}/destroy', 'destroy')->name('authors.destroy');
        Route::get('/author/create', 'create')->name('author.create');
        Route::post('/authors/store', 'store')->name('authors.store');
        Route::put('/authors/{author}/save', 'save')->name('authors.save');
    });
});

Route::controller(GenreController::class)->group(function () {
    Route::middleware(['auth', 'verified'])->group(function () {
        Route::get('/genres', 'index')->name('genres.index');
        Route::get('/genres/{genre}', 'show')->name('genres.show');
        Route::get('/genres/{genre}/update', 'update')->name('genres.update');
        Route::get('/genres/{genre}/delete', 'delete')->name('genres.delete');
        Route::delete('/genres/{genre}/destroy', 'destroy')->name('genres.destroy');
        Route::get('/genre/create', 'create')->name('genre.create');
        Route::post('/genres/store', 'store')->name('genres.store');
        Route::put('/genres/{genre}/save', 'save')->name('genres.save');
    });
});

Route::post('/tokens/create', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);

    return ['token' => $token->plainTextToken];
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
