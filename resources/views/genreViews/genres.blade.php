<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Жанры') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 flex justify-between">
                    @auth
                    <div>
                        @foreach($genres as $genre)
                        <div> <a href="{{route('genres.show', ['genre' => $genre->id])}}">{{$genre->name}}</a></div>
                        @endforeach
                    </div>
                    <a href="{{route('genre.create')}}">
                        <x-primary-button type='button'>{{ __('Добавить жанр') }}</x-primary-button>
                    </a>
                    @else
                    Инфа не для авторизованных
                    @endauth
                </div>
            </div>
        </div>
    </div>
</x-app-layout>