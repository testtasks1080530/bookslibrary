<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @if($type === 'create') Создание жанра @endif
            @if($type === 'update') Изменение жанра @endif
            @if($type === 'delete') Удаление жанра @endif
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    @auth
                    @if ($type === 'update')
                    <x-genre.form action="{{route('genres.save', ['genre' => $genre->id])}}" method='put' :type=$type :genre='$genre' />
                    @endif
                    @if ($type === 'delete')
                    <x-genre.alert-delete :genre='$genre'/>
                    @endif
                    @if ($type === 'create')
                    <x-genre.form action="{{route('genres.store')}}" method='post' :type=$type />
                    @endif
                    @else
                    Инфа не для авторизованных
                    @endauth
                </div>
            </div>
        </div>
    </div>

</x-app-layout>