@props(['author' => null])

<x-form action="{{route('authors.destroy', ['author' => $author->id])}}" method='delete' class="p-6" :author='$author'>
    <h2 class="text-lg font-medium text-gray-900">
        Вы действительно хотите удалить {{$author->name}} ?
    </h2>
    <div class="mt-6 flex justify-end">
        <a href="{{route('authors.show', ['author' => $author->id])}}">
            <x-secondary-button>{{ __('Отмена') }}</x-secondary-button>
        </a>
        <x-danger-button class="ml-3">
            {{ __('Удалить') }}
        </x-danger-button>
    </div>
</x-form>