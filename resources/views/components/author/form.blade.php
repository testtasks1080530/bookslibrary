@props(['type' => 'create','author' => null])


<x-form {{ $attributes }} class="mt-6 space-y-6">
    <div>
        <x-input-label for="author_name" :value="__('Имя автора')" />
        @if ($type === 'create')
        <x-text-input id="author_name" name="name" type="text" class="mt-1 block w-full" :value="old('name')" required autofocus autocomplete="author_name" />
        @elseif ($type === 'update')
        <x-text-input id="author_name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $author->name)" required autofocus autocomplete="author_name" />
        @endif
        <x-input-error class="mt-2" :messages="$errors->get('name')" />
    </div>
    <div class="flex items-center gap-4">
        @if ($type === 'create')
        <x-primary-button>{{ __('Создать') }}</x-primary-button>
        @elseif ($type === 'update')
        <x-primary-button>{{ __('Изменить') }}</x-primary-button>
        @endif
        <a @if ($type==='create' ) href="{{route('authors.index')}}" @elseif ($type==='update' ) href="{{route('authors.show', ['author' => $author->id])}}" @endif>
            <x-secondary-button>{{ __('Отмена') }}</x-secondary-button>
        </a>
    </div>
</x-form>