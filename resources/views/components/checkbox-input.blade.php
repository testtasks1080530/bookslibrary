@props(['all' => [], 'uncheckedItem' => []])

@foreach ($all as $item)
@if ($uncheckedItem)  
    @if ($uncheckedItem->contains($item))
        <input id="{{$item->name}}" name="{{$item->name}}" value="{{$item->id}}" @checked(old($item->name, false )) type="checkbox" class="ml-2 inline-block"  />
    @else
        <input id="{{$item->name}}" name="{{$item->name}}" value="{{$item->id}}" @checked(old($item->name, true )) type="checkbox" class="ml-2 inline-block"  />
    @endif
@else
    <input id="{{$item->name}}" name="{{$item->name}}" value="{{$item->id}}" @checked(old($item->name, false )) type="checkbox" class="ml-2 inline-block"  />
@endif
    <label for="{{$item->name}}">{{$item->name}}</label>
@endforeach