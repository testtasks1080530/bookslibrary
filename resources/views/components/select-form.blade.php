@props(['values' => [], 'valueDefault' => ''])

<select {{$attributes}}>
    @foreach ($values as $val)    
        @if ($val->value === $valueDefault)
            <option selected value="{{ $val->value }}">{{ $val->name }}</option>
        @else
            <option value="{{ $val->value }}">{{ $val->name }}</option>
        @endif
    @endforeach        
</select>