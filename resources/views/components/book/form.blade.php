@props(['type' => 'create','book' => null, 'author' => null, 'genre' => null, 'genresUnchecked' => [], 'genresAll' => []])


<x-form {{ $attributes }} class="mt-6 space-y-6">
    <div>
        <x-input-label for="book_name" :value="__('Название книги')" />
        @if ($type === 'create')
        <x-text-input id="book_name" name="name" type="text" class="mt-1 block w-full" :value="old('name')" required autofocus autocomplete="book_name" />
        @elseif ($type === 'update')
        <x-text-input id="book_name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $book->name)" required autofocus autocomplete="book_name" />
        @endif
        <x-input-error class="mt-2" :messages="$errors->get('name')" />
    </div>
    <div>
        <x-input-label for="book_type" :value="__('Тип книги')" />
        @if ($type === 'create')
        <x-select-form id="book_types" name="type" class="mt-1 block w-1/2" :valueDefault="old('type')" :values="App\Enums\BookTypes::cases()" />
        @elseif ($type === 'update')
        <x-select-form id="book_types" name="type" class="mt-1 block w-1/2" :valueDefault="old('type', $book->type)" :values="App\Enums\BookTypes::cases()" />
        @endif
        <x-input-error class="mt-2" :messages="$errors->get('type')" />
    </div>
    <div>
        <x-input-label for="book_author" :value="__('Автор')" />
        @if ($type === 'create')
        <x-text-input id="book_author" name="author" type="text" class="mt-1 block w-full" :value="old('author')" required autofocus autocomplete="book_author" />
        @elseif ($type === 'update')
        <x-text-input id="book_author" name="author" type="text" class="mt-1 block w-full" :value="old('author', $author->name)" required autofocus autocomplete="book_author" />
        @endif
        <x-input-error class="mt-2" :messages="$errors->get('author')" />
    </div>
    <div>
        <x-input-label for="book_genre" :value="__('Жанр')" />
        <x-checkbox-input id="book_genre" name="genre" class="mt-2 inline w-full" :all="$genresAll" :uncheckedItem="$genresUnchecked" required />
        <x-input-error class="mt-2" :messages="$errors->get('genre')" />
    </div>
    <div class="flex items-center gap-4">
        @if ($type === 'create')
        <x-primary-button>{{ __('Создать') }}</x-primary-button>
        @elseif ($type === 'update')
        <x-primary-button>{{ __('Изменить') }}</x-primary-button>
        @endif
        <a @if ($type==='create' ) href="{{route('books.index')}}" @elseif ($type==='update' ) href="{{route('books.show', ['book' => $book->id])}}" @endif>
            <x-secondary-button>{{ __('Отмена') }}</x-secondary-button>
        </a>
    </div>
</x-form>