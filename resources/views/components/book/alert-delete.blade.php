@props(['book' => null])

<x-form action="{{route('books.destroy', ['book' => $book->id])}}" method='delete' class="p-6" :book='$book'>
    <h2 class="text-lg font-medium text-gray-900">
        Вы действительно хотите удалить {{$book->name}} ?
    </h2>
    <div class="mt-6 flex justify-end">
        <a href="{{route('books.show', ['book' => $book->id])}}">
            <x-secondary-button>{{ __('Отмена') }}</x-secondary-button>
        </a>
        <x-danger-button class="ml-3">
            {{ __('Удалить') }}
        </x-danger-button>
    </div>
</x-form>