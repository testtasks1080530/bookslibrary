@props(['type' => 'create','genre' => null])


<x-form {{ $attributes }} class="mt-6 space-y-6">
    <div>
        <x-input-label for="genre_name" :value="__('Название жанра')" />
        @if ($type === 'create')
        <x-text-input id="genre_name" name="name" type="text" class="mt-1 block w-full" :value="old('name')" required autofocus autocomplete="genre_name" />
        @elseif ($type === 'update')
        <x-text-input id="genre_name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $genre->name)" required autofocus autocomplete="genre_name" />
        @endif
        <x-input-error class="mt-2" :messages="$errors->get('name')" />
    </div>
    <div class="flex items-center gap-4">
        @if ($type === 'create')
        <x-primary-button>{{ __('Создать') }}</x-primary-button>
        @elseif ($type === 'update')
        <x-primary-button>{{ __('Изменить') }}</x-primary-button>
        @endif
        <a @if ($type==='create' ) href="{{route('genres.index')}}" @elseif ($type==='update' ) href="{{route('genres.show', ['genre' => $genre->id])}}" @endif>
            <x-secondary-button>{{ __('Отмена') }}</x-secondary-button>
        </a>
    </div>
</x-form>