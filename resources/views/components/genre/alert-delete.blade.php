@props(['genre' => null])

<x-form action="{{route('genres.destroy', ['genre' => $genre->id])}}" method='delete' class="p-6" :genre='$genre'>
    <h2 class="text-lg font-medium text-gray-900">
        Вы действительно хотите удалить {{$genre->name}} ?
    </h2>
    <div class="mt-6 flex justify-end">
        <a href="{{route('genres.show', ['genre' => $genre->id])}}">
            <x-secondary-button>{{ __('Отмена') }}</x-secondary-button>
        </a>
        <x-danger-button class="ml-3">
            {{ __('Удалить') }}
        </x-danger-button>
    </div>
</x-form>