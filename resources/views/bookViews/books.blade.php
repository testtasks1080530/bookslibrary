<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Книги') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    @auth
                    <div class="mb-6 pb-3">
                        <x-form action="{{route('books.search')}}" method='get'>
                            <div class="flex justify-start gap-4">
                                <div class="mr-2">
                                    <label for="search_book" class=" font-semibold text-sm text-gray-700 uppercase mr-2">ПОИСК:</label>
                                    <input id="search_book" name="search_book" type="text" placeholder="Название книги" class="border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm w-48" value="{{old('search_book', Request::query('search_book'))}}" autofocus autocomplete="search_book" />
                                </div>
                                <div class="mr-2">
                                    <label for="filter_author" class=" font-semibold text-sm text-gray-700 uppercase mr-2">Автор:</label>
                                    <select id="filter_author" name="filter_author" class="border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm w-48">
                                        <option value="all">все</option>
                                            @foreach ($authors as $author)
                                                @if ($author->id == Request::query('filter_author'))
                                                <option selected value="{{ $author->id }}">{{$author->name}}</option>
                                                @else
                                                <option value="{{ $author->id }}">{{$author->name}}</option>
                                                @endif
                                            @endforeach
                                    </select>
                                </div>
                                <div class="mr-2">
                                    <label for="filter_genre" class=" font-semibold text-sm text-gray-700 uppercase mr-2">Жанр:</label>
                                    <select id="filter_genre" name="filter_genre" class="border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm w-48">
                                        <option value="all">все</option>
                                        @foreach ($genres as $genre)
                                            @if ($genre->id == Request::query('filter_genre'))
                                            <option selected value="{{ $genre->id }}">{{$genre->name}}</option>
                                            @else
                                            <option value="{{ $genre->id }}">{{$genre->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mr-2 flex items-center">
                                <label for="sort_book">А-я</label>
                                <input id="sort_book" name="sort_book" @checked(old('sort_book', Request::query('sort_book'))) type="checkbox" class="ml-2 inline"  />
                                </div>
                                <x-primary-button>{{ __('Применить') }}</x-primary-button>
                                <a href="{{route('books.index')}}">
                                    <x-secondary-button>Сбросить</x-secondary-button>
                                </a>
                            </div>
                        </x-form>
                    </div>
                    <div class="flex justify-between pt-6">
                        <div class="w-3/4 block">
                            @foreach($books as $book)
                            <a href="{{route('books.show', ['book' => $book->id])}}">
                                <div class="mb-6 border-b-2">
                                    <p class="font-semibold text-lg">"{{$book->name}}"</p>
                                    <p class="font-base text-sm">{{$book->author->name}}</p>
                                    @foreach($book->genres as $genre)
                                    <span class="font-base text-sm text-gray-600">#{{$genre->name}}</span>
                                    @endforeach
                                    <p class="font-base text-sm mt-3 text-gray-400">{{$book->getCreatedAtAttribute()}}</p>
                                </div>
                            </a>
                            @endforeach
                        </div>
                        <a href="{{route('book.create')}}">
                            <x-secondary-button type='button'>{{ __('Добавить книгу') }}</x-secondary-button>
                        </a>
                    </div>
                    @else
                    Инфа не для авторизованных
                    @endauth
                </div>
            </div>
        </div>
    </div>
</x-app-layout>