<x-app-layout>
    <x-slot name="header">
        <div class="w-full flex justify-between">
            <div>
                <h2 class="font-normal text-base text-gray-800 leading-tight">
                    Книга
                </h2>
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ $book->name }}
                </h2>
            </div>
            <div>
                <a href="{{route('books.index')}}">
                    <x-secondary-button type='button'>{{ __('<- Назад') }}</x-secondary-button>
                </a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="flex justify-between p-6 text-gray-900">
                    @auth
                    <div>
                        <a href="{{route('books.update', ['book' => $book->id])}}">
                            <x-secondary-button type='button'>{{ __('Изменить') }}</x-secondary-button>
                        </a>
                        <a href="{{route('books.delete', ['book' => $book->id])}}">
                            <x-danger-button type='button'>{{ __('Удалить') }}</x-danger-button>
                        </a>
                    </div>
                    @else
                    Информация не для авторизованных
                    @endauth
                </div>
            </div>
        </div>
    </div>
</x-app-layout>