<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @if($type === 'create') Создание книги @endif
            @if($type === 'update') Изменение книги @endif
            @if($type === 'delete') Удаление книги @endif
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    @auth
                    @if ($type === 'update')
                    <x-book.form action="{{route('books.save', ['book' => $book->id])}}" method='put' :type=$type :book='$book' :author='$author' :genresUnchecked='$genresUnchecked' :genresAll='$genresAll' />
                    @endif
                    @if ($type === 'delete')
                    <x-book.alert-delete :book='$book'/>
                    @endif
                    @if ($type === 'create')
                    <x-book.form action="{{route('books.store')}}" method='post' :type=$type :genresAll='$genresAll' />
                    @endif
                    @else
                    Инфа не для авторизованных
                    @endauth
                </div>
            </div>
        </div>
    </div>

</x-app-layout>