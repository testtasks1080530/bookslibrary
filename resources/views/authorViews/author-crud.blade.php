<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @if($type === 'create') Создание автора @endif
            @if($type === 'update') Изменение автора @endif
            @if($type === 'delete') Удаление автора @endif
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    @auth
                    @if ($type === 'update')
                    <x-author.form action="{{route('authors.save', ['author' => $author->id])}}" method='put' :type=$type :author='$author' />
                    @endif
                    @if ($type === 'delete')
                    <x-author.alert-delete :author='$author'/>
                    @endif
                    @if ($type === 'create')
                    <x-author.form action="{{route('authors.store')}}" method='post' :type=$type />
                    @endif
                    @else
                    Инфа не для авторизованных
                    @endauth
                </div>
            </div>
        </div>
    </div>

</x-app-layout>