<?php

namespace Database\Factories;

use App\Models\Authors;
use App\Models\Books;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Books>
 */
class BooksFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Books::class;
    protected $enumTypes = ['graphic', 'digital', 'print'];
    public function definition()
    {
        return [
            'name'=>$this->faker->text(20) ,
            'type'=>$this->enumTypes[rand(0,2)],
            'author_id'=>Authors::get()->random()->id
        ];
    }
}
