<?php

namespace Database\Factories;

use App\Models\Genres;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Validation\Rules\Unique;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Genres>
 */
class GenresFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Genres::class;

    public function definition()
    {
        return [
            'name'=>$this->faker->unique()->word()
        ];
    }
}
