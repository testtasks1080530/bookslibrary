<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Authors;
use App\Models\Books;
use App\Models\Genres;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Authors::factory(30)->create();
        $genres = Genres::factory(30)->create();
        $books = Books::factory(30)->create();
        Log::channel('booksLogs')->info('SEED::run()');
        foreach($books as $book) {
            $genreIds = $genres->random(rand(1,5))->pluck('id');
            $book->genres()->attach($genreIds);
        }
    }
}
