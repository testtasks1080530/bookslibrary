<?php

use App\Enums\BookTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Validation\Rules\Enum;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('type', array_column(BookTypes::cases(), 'value'));
            $table->unsignedBigInteger('author_id');
            $table->index('author_id', 'author_book_idx');
            $table->foreign('author_id', 'author_book_fk')->on('authors')->references('id')->onDelete('cascade');
            $table->timestamps();
        });
        Log::channel('booksLogs')->info('MIGRATE::up()');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
        Log::channel('booksLogs')->info('MIGRATE::down()');
    }
};
